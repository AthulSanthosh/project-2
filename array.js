// for each 
let for_each = function each(elements, for_each_cb) 
    {
    if (elements === undefined) 
     {
        return "array is undefined"
     }
    else if (elements.length === 0)   
     {
        return "Array is empty"
     }
     else if (for_each_cb === undefined)
     {
      return "call back function error"
     }
     else 
     {       
        for (let index = 0 ; index < elements.length; index++)  // print the element and index
            {
              console.log(elements[index],for_each_cb(index))  
            }
     } 
    }


// map 

let map = function each(elements, map_cb) 
    {
    if (elements === undefined) 
     {
        return "array is undefined"
     }
    else if (elements.length === 0)    
     {
        return "Array is empty"
     }
     else if (map_cb === undefined)
     {
      return "call back function error"
     }
     else 
     {
        map_result = []
        for (let index = 0 ; index < elements.length; index++)  // print the element and index
        {
           map_result.push(map_cb(elements[index])) 
        }
        return map_result
     } 
    }

// reduce

let reduce = function reduce(elements, cb, startingValue ) 
{
   {
      if (elements === undefined) 
       {
          console.log("array is undefined")
       } 
      else if (elements.length === 0)     
       {
          console.log("Array is empty")
       }
       else if (cb === undefined){
          console.log( "call back function error")
       }
       else 
       { 
          total_sum = 0
          for (let index = 0 ; index < elements.length; index++)  // print the element and index
          {
            startingValue = elements[index]
             total_sum = cb(total_sum, startingValue)
          }
          return total_sum
       } 
      }
 }


// find 

let find = function find(elements,find_cb) 
{  
      if (elements === undefined) 
       {
         return "array is undefined"           
       }  
      else if (elements.length === 0)    
       {
          return "Array is empty"        
       }
       else if(find_cb === undefined)
       {
         return "Call back function error"       
       }        
      for (let index = 0 ; index < elements.length; index++)  // print the element and index
          {
             if (find_cb (elements[index])) 
             {
                return `Element is present`
             }
          }
       {
         return 'undefined'
       }
          
   
}
  
// filter 


let filter = function(elements , filter_cb)
{   
   if (elements === undefined) 
    {
      return "array is undefined"       
    }
   else if (elements.length === 0)
   
    {
      return "Array is empty"      
    }
    else if(filter_cb === undefined)
    {
      return "Call back function error"
    } 
   filtered_arr = []
   for (let index = 0 ; index < elements.length; index++)         // print the element and index
       {          
          if (filter_cb (elements[index])) 
          {
             filtered_arr.push(elements[index])
          }
       }
   if (filtered_arr) {
      return filtered_arr
   }
   else{
      return "undefined"
   }
}

//flatten 

let flatten = function (arr) 
{
   if ((arr === undefined) || arr.length === 0)
   {
      return "Invalid Array"
   }
   flatten_arr = []
   for (index = 0 ; index < arr.length ; index++)
   {
   flatten_arr.push(flatten_helper(arr[index]))
   }  
   return flatten_arr

}
let flatten_helper = function (element)    // recursive function to fetch the element
{ 
   if(Array.isArray(element))
   {
      return flatten_helper(element[0])      
    }
   else
   { 
      return element
   }

}
module.exports = {for_each,  map ,reduce , find, filter, flatten };