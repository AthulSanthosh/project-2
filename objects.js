// object returning keys as a list 
let obj_keys =   function object_keys(obj){
      //console.log(Object.keys(obj))                     using the Object.keys, built in function
      if (obj === undefined)
      {
        return "undefined object"   
      }
      keys = []
      for (let key in obj)
      {
          keys.push(key)
      }
      return keys
      }

// return object values
let obj_values = function object_values(obj)
{
    //console.log(Object.values(obj))               
    if (obj === undefined)
    {
        return "undefined object"      
    }
    values = []
    for (let key in obj)
    {
        values.push(obj[key])
    }
    return values
}

let obj_map = function map(obj,cb)
   {      
    if (obj === undefined)
    {
    return "undefined object"
    }
    if (cb === undefined)
    {
        return "call back function is not provided"
    }
    for (let key in obj)
    {
        if (typeof obj[key] === 'string')
        {
        obj[key] = cb(obj[key])
        }
    }
}
// pair function

let obj_pair = function pair(obj)
{
    if(obj === undefined)
    {
    return "undefined object"
    }
    key_value_pair = []
    // parse through the elements add a list of key and obj[key]
    for (let key in obj)
    {
        key_value_pair.push([key,obj[key]])
    }
    return key_value_pair

}

// invert function

let obj_invert = function invert(obj) 
{
    if(obj === undefined)
    {
        return "object not defined"
    }
    obj_invert_copy = {}
    for(let key in obj)
    {
    obj_invert_copy[obj[key]] = key                                            
    }
    return obj_invert_copy

}

// default function
let obj_default = function default_(obj,default_para)
{
    // base checks
    if(obj === undefined)
    {
        return "object  undefined"        
    }
    else if (default_para === undefined)
    {
        return "No default Parameter specified"     
    }
    // main function
    for (let key in obj)
    {
        if (obj[key] === undefined)
        {
            if (key in default_para)
            {
                obj[key] = default_para[key]
            }
        }
    }
}


module.exports = {
    keys:obj_keys,
    values:obj_values,
    map:obj_map,
    pairs:obj_pair,
    invert:obj_invert,
    default_:obj_default
}