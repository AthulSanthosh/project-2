let value = 0
let count = function () {
    return {
      increment: function () 
      { value += 1; },
      decrement: function() 
      { value -= 1; },
      value : function()
      {return value}
    };
  }
  

let limitFunction = function (cb, n)
{
  if (cb === undefined){
    return null
  }
  count = 1
  while (count <=n) {
    cb(count)
    count += 1 
    if (count === n)
    {
      return `Function has executed ${n} times`
    }
  }
}

cache = {}                                                      // cache declared in global scope
let cacheFunction = function (cb,number) 
{
  //console.log(cache)
  if (number in cache)
  {
    return "This is is from cache " + cache[number]
  }
  else{
    cache[number] = cb(number)
    return cache[number]
  }
}

module.exports = {
    count,
    limitFunction,
    cacheFunction
}