const from_array = require('./array.js')
const  test_input  = require('./input.js')
items = test_input.items
arr = test_input.nest


console.log("For each function ")                 // for_each function
let for_each_cb = index => index                  // function to return the index passed
from_array.for_each(items,for_each_cb)




console.log("\n Map function ")                 // map function to produce square array from existing array and return
let map_cb = function(x)                          // function to caluclate the sqaure
{    
  return x * x
}  
let map = from_array.map(items,map_cb)
console.log(map)



console.log("\n Reduce function ")                // reduce function
let reduce_cb = (startingValue,element) => startingValue + element  // function adds previous sum with current element
let reduce = from_array.reduce(items,reduce_cb)
console.log(reduce)



console.log("\n Find function ")                  // find function
let find_cb = (element) => {                      // cb function which returns true if the element is 3
  if (element === 3)
  {
    return true
  }
}
let find = from_array.find(items , find_cb)
console.log(find)


console.log("\n Filter function ")                // filter function 
let filter_cb = (element) => {                    // function to pass only even numbers
  if (element % 2 === 0)
  {
    return true
  }
}
let filter = from_array.filter(items,filter_cb)
console.log(filter)



console.log("\n Flatten Function  ")             // flatten function
let flatten= from_array.flatten(arr)
console.log(flatten)
