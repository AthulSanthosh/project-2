const closure = require('./closure')


// counter
console.log("\nCounter ")
console.log(closure.count().value())                       //value 0 
closure.count().increment()                                //value incremented to 1
closure.count().increment()                                //value incremented to 2
console.log(closure.count().value())                       //value 2
closure.count().decrement()                                //value decremented to 1
console.log(closure.count().value())                       //value 1



// limit
console.log("\nLimit ")
let cb = number => "hey " + number
n = 5
let limitFunction = closure.limitFunction(cb,n)
console.log(limitFunction)


// cache 
console.log("\nCache ")
const square = number => number * number                     // cb function which calculates square
console.log(closure.cacheFunction(square,5))
console.log(closure.cacheFunction(square,6))
console.log(closure.cacheFunction(square,5))                // result from cache


