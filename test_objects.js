const input = require('./input.js')
const object = input.object
const object_methods = require('./objects.js')

// keys 
// object keys
console.log("\n Object keys")
let object_keys = object_methods.keys(object)
console.log(object_keys)



// values 
console.log("\n Object values ")
let object_values = object_methods.values(object)
console.log(object_values)



// map
// define the call back function, if the value is string , changes it to upper case
console.log("\n Object Map ")
let map_cb = (element) => element.toUpperCase()
object_methods.map(object,map_cb)
let object_value_map_test = object_methods.values(object)
console.log(object_value_map_test)


//pair 
console.log("\n Object Pair ")
let pair = object_methods.pairs(object)
console.log(pair)


console.log("\nObject invert  ")
// invert 
let invert = object_methods.invert(object)
console.log(invert)



// default props 
// Fill in undefined properties that match properties on the `defaultProps` parameter object.
// added an undefine value email
console.log("\nObject default props  ")
object.email = undefined
default_props = {email : "bruce@batworld.com",vehicle: "Batmobile"}
object_methods.default_(object,default_props)
console.log(object)
