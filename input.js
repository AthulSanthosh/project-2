const items = [1,2,3,4,5,5];
const nestedArray = [1,[[2]],[[3]],[[[4]]]];
const testObject = { name: 'Bruce Wayne', age: 36, location:'Gotham'}

// export both the modules 
module.exports = {
    items:items,
    nest:nestedArray,
    object:testObject
}